package modelObject;

import sun.util.resources.LocaleData;

import java.util.List;

public class Order {
   private  List<Product> UserProduct;

    private LocaleData date;

    public List<Product> getUserProduct() {
        return UserProduct;
    }

    public void setUserProduct(List<Product> userProduct) {
        UserProduct = userProduct;
    }

    public LocaleData getDate() {
        return date;
    }

    public void setDate(LocaleData date) {
        this.date = date;
    }
}


