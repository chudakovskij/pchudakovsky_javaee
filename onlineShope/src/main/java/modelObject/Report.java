package modelObject;

public class Report {

   private UserShope name;
    private int ozenka;
    private String userReport;


    public UserShope getName() {
        return name;
    }

    public void setName(UserShope name) {
        this.name = name;
    }

    public int getOzenka() {
        return ozenka;
    }

    public void setOzenka(int ozenka) {
        this.ozenka = ozenka;
    }

    public String getUserReport() {
        return userReport;
    }

    public void setUserReport(String userReport) {
        this.userReport = userReport;
    }
}
